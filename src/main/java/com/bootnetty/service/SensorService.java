package com.bootnetty.service;

import com.bootnetty.entity.Sensors;
import com.bootnetty.mapper.SensorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by stevenfenCai on 2019-11-13.
 */
@Service
public class SensorService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SensorMapper sensorMapper;

    public void insertSensor(Sensors sensors){
        sensorMapper.insertSensor(sensors);
        logger.info("插入蜂箱"+sensors.getSerialNum()+"一条新数据");

    }
}
