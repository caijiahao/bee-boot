package com.bootnetty.service;

import com.bootnetty.entity.Result;
import com.bootnetty.entity.Station;
import com.bootnetty.mapper.StationMapper;
import com.bootnetty.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stevenfenCai on 2019-11-11.
 */
@Service
public class StationService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    StationMapper stationMapper;

    public void UpdateStaion(Station station) {
        stationMapper.updateStaion(station);
    }

    public Result getStationConfig(Long pid){
        List<Station> stationList = stationMapper.selectSationByPid(pid);
        if(stationList!=null&&stationList.size()>0){
            Result result = new Result();
            List<String> IDs = new ArrayList<>();
            List<String> Modes = new ArrayList<>();
            List<String> expectTemperatures = new ArrayList<>();
            List<String> expectHumiditys = new ArrayList<>();
            String ID = "";
            String Mode = "";
            String expectTemperature = "";
            String expectHumidity = "";

            for(Station station:stationList){
                ID = String.format("%016x", station.getSerialNum());
                String HMode = station.getHMode()==78?"0":"1";
                String TMode = station.getTMode()==78?"0":"1";
                String TSatus = station.getTSatus()==76?"0":"1";
                String HSatus = station.getHSatus()==76?"0":"1";
                Mode = String.format("%04x",Integer.parseInt(HSatus+TSatus+HMode+TMode,2));
                expectTemperature = String.format("%04x",Integer.parseInt(new java.text.DecimalFormat("0").format(station.getExpectTemperature()*10)));
                expectHumidity  = String.format("%04x",Integer.parseInt(new java.text.DecimalFormat("0").format(station.getExpectHumidity()*10)));
                IDs.add(StringUtils.reverse(ID));
                Modes.add(StringUtils.reverse(Mode));
                expectTemperatures.add(StringUtils.reverse(expectTemperature));
                expectHumiditys.add(StringUtils.reverse(expectHumidity));
            }
            result.setIDs(IDs);
            result.setModes(Modes);
            result.setExpectTemperature(expectTemperatures);
            result.setExpectHumidity(expectHumiditys);
            logger.info("成功获取基地"+pid+"的所有蜂箱参数！！！！");
            return result;
        }
        return null;
    }
}
