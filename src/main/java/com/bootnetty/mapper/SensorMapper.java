package com.bootnetty.mapper;

import com.bootnetty.entity.Sensors;
import org.springframework.stereotype.Repository;


/**
 * Created by stevenfenCai on 2019-11-13.
 */
@Repository
public interface SensorMapper {
    /**
     * 插入蜂箱环境数据
     * @param sensor
     */
    void insertSensor(Sensors sensor);
}
