package com.bootnetty.mapper;

import com.bootnetty.entity.Station;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by stevenfenCai on 2019-11-11.
 */
@Repository
public interface StationMapper {
    /**
     * 更新蜂箱信息
     * @param station
     */
    void updateStaion(Station station);

    /**
     * 根据蜂箱序列号查询蜂箱
     * @param pid
     * @return
     */
    List<Station> selectSationByPid(Long pid);
}
