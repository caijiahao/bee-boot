package com.bootnetty.quartz;

import com.aliyun.oss.OSSClient;
import com.bootnetty.oss.AliyunOSSClient;
import com.bootnetty.oss.OSSClientConstants;
import com.bootnetty.utils.FileUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.bootnetty.oss.OSSClientConstants.BACKET_NAME;
import static com.bootnetty.oss.OSSClientConstants.FOLDER;

/**
 * Created by stevenfenCai on 2019-11-14.
 */
public class SampleJob extends QuartzJobBean {
    private String name;

    @Autowired
    private AliyunOSSClient aliyunOSSClient;

    @Autowired
    private OSSClientConstants ossClientConstants;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void setName(String name) {
        this.name = name;
    }


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //初始化OSSClient
        OSSClient ossClient= AliyunOSSClient.getOSSClient();
        //上传文件
        ArrayList<String> files = new ArrayList<>();
        FileUtils.getAllFileName(ossClientConstants.rootPath,files);
        for(String filename:files){
            if(filename.indexOf(".mp4")!=-1) {
                System.out.println("filename:" + filename.substring(8,18));
                String fileDir = filename.substring(8,18)+"/";
                //判断是否存在该文件夹不存在就新增
                aliyunOSSClient.createFolder(ossClient,BACKET_NAME,FOLDER+fileDir);
                File filess=new File(filename);
                String md5key = aliyunOSSClient.uploadObject2OSS(ossClient, filess, BACKET_NAME, FOLDER+fileDir);
                logger.info("上传后的文件MD5数字唯一签名:" + md5key);
            }
        }
    }
}
