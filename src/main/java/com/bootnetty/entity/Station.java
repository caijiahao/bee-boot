package com.bootnetty.entity;

/**
 * Created by stevenfenCai on 2019-11-11.
 */
public class Station {
    private Integer active;

    public Integer TMode;

    public Integer HMode;

    public Integer TSatus;

    public Integer HSatus;

    public Double expectTemperature;

    public Double expectHumidity;

    public Long serialNum;

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getTMode() {
        return TMode;
    }

    public void setTMode(Integer TMode) {
        this.TMode = TMode;
    }

    public Integer getHMode() {
        return HMode;
    }

    public void setHMode(Integer HMode) {
        this.HMode = HMode;
    }

    public Integer getTSatus() {
        return TSatus;
    }

    public void setTSatus(Integer TSatus) {
        this.TSatus = TSatus;
    }

    public Integer getHSatus() {
        return HSatus;
    }

    public void setHSatus(Integer HSatus) {
        this.HSatus = HSatus;
    }

    public Double getExpectTemperature() {
        return expectTemperature;
    }

    public void setExpectTemperature(Double expectTemperature) {
        this.expectTemperature = expectTemperature;
    }

    public Double getExpectHumidity() {
        return expectHumidity;
    }

    public void setExpectHumidity(Double expectHumidity) {
        this.expectHumidity = expectHumidity;
    }

    public Long getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(Long serialNum) {
        this.serialNum = serialNum;
    }
}
