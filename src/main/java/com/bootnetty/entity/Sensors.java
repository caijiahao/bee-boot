package com.bootnetty.entity;

/**
 * Created by stevenfenCai on 2019-11-11.
 */
public class Sensors {
    private int AutoID;
    private String CreateDate;
    private String UpdateDate;
    private String TS;
    private int Deleted;
    private double temperature;
    private double humidity;
    private double TMode;
    private double HMode;
    private Long serialNum;

    public int getAutoID() {
        return AutoID;
    }

    public void setAutoID(int autoID) {
        AutoID = autoID;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public String getTS() {
        return TS;
    }

    public void setTS(String TS) {
        this.TS = TS;
    }

    public int getDeleted() {
        return Deleted;
    }

    public void setDeleted(int deleted) {
        Deleted = deleted;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getTMode() {
        return TMode;
    }

    public void setTMode(double TMode) {
        this.TMode = TMode;
    }

    public double getHMode() {
        return HMode;
    }

    public void setHMode(double HMode) {
        this.HMode = HMode;
    }

    public Long getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(Long serialNum) {
        this.serialNum = serialNum;
    }
}
