package com.bootnetty.entity;

import java.util.List;

/**
 * Created by stevenfenCai on 2019-11-12.
 */
public class Result {
    private String dataID;

    private String timeLine;

    private String pid;

    private String dataType;

    private List<String> IDs;

    private List<String> Modes;

    private List<String> expectTemperature;

    private List<String> expectHumidity;

    public String getDataID() {
        return dataID;
    }

    public void setDataID(String dataID) {
        this.dataID = dataID;
    }

    public String getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(String timeLine) {
        this.timeLine = timeLine;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public List<String> getIDs() {
        return IDs;
    }

    public void setIDs(List<String> IDs) {
        this.IDs = IDs;
    }

    public List<String> getModes() {
        return Modes;
    }

    public void setModes(List<String> modes) {
        Modes = modes;
    }

    public List<String> getExpectTemperature() {
        return expectTemperature;
    }

    public void setExpectTemperature(List<String> expectTemperature) {
        this.expectTemperature = expectTemperature;
    }

    public List<String> getExpectHumidity() {
        return expectHumidity;
    }

    public void setExpectHumidity(List<String> expectHumidity) {
        this.expectHumidity = expectHumidity;
    }
}
