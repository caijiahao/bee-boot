package com.bootnetty.channel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by stevenfenCai on 2019-11-13.
 */
public class MessagePacketDecoder extends ByteToMessageDecoder {

    public MessagePacketDecoder() throws Exception
    {
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        try {
            if (byteBuf.readableBytes() > 0) {
                // 待处理的消息包
                byte[] bytesReady = new byte[byteBuf.readableBytes()];
                byteBuf.readBytes(bytesReady);
                //这之间可以进行报文的解析处理
                list.add(bytesReady );
            }
        }finally {

        }
    }
}
