package com.bootnetty.channel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * Created by stevenfenCai on 2019-11-13.
 */
public class MessagePacketEncoder extends MessageToByteEncoder<Object> {
    public MessagePacketEncoder()
    {
    }
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object msg, ByteBuf byteBuf) throws Exception {
        try {
            //在这之前可以实现编码工作。
            byteBuf.writeBytes((byte[])msg);
        }finally {

        }
    }
}
