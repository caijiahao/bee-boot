package com.bootnetty.oss;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by stevenfenCai on 2019-10-29.
 */
@Component
public class OSSClientConstants {

    //阿里云API的外网域名
    public static final String ENDPOINT = "oss-cn-shenzhen.aliyuncs.com";

    //阿里云API的密钥Access Key ID
    public static final String ACCESS_KEY_ID = "LTAIFfhFiByzsbyK";

    //阿里云API的密钥Access Key Secret
    public static final String ACCESS_KEY_SECRET = "i9sqeFSmMX5qANsMzDusq3ibFyM6Ls";

    //阿里云API的bucket名称
    public static final String BACKET_NAME = "scau-wsn640-cjh";//"uploadpicture";

    //阿里云API的文件夹名称
    public static final String FOLDER="bee/";

    //本地视频根目录
    @Value("${oss.rootpath}")
    public String rootPath;
}
