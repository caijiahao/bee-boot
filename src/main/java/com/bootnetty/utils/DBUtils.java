package com.bootnetty.utils;

import com.bootnetty.entity.Result;
import com.bootnetty.entity.Sensors;
import com.bootnetty.entity.Station;
import com.bootnetty.entity.User;
import com.bootnetty.service.SensorService;
import com.bootnetty.service.StationService;
import com.bootnetty.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class DBUtils {

    @Autowired
    private UserService userService;

    @Autowired
    private StationService stationService;

    @Autowired
    private SensorService sensorService;

    public static DBUtils dbUtils;

    @PostConstruct
    public void init() {
        dbUtils = this;
    }

    public static User test(int id) {

       // 调用service的方法
      return dbUtils.userService.Sel(id);
    }


    public static void updateStation(Station station){
        dbUtils.stationService.UpdateStaion(station);
    }

    public static Result getStationConfig(Long pid){
        return dbUtils.stationService.getStationConfig(pid);
    }


    public static void insertSensor(Sensors sensors){
        dbUtils.sensorService.insertSensor(sensors);
    }
}
