package com.bootnetty;

import com.bootnetty.server.BootNettyServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@MapperScan("com.bootnetty.mapper") //扫描的mapper
@SpringBootApplication(scanBasePackages = "com")
public class App {

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(App.class);
        app.setWebApplicationType(WebApplicationType.NONE);//不启动web服务
        app.run(args);
        /**
         * 启动netty服务端服务
         */
        System.out.println( "Started Server!" );
        new BootNettyServer().bind(8888);
    }
}
